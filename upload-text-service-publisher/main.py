from encodings import utf_8
from fastapi import FastAPI, File, UploadFile, Depends
from fastapi.middleware.cors import CORSMiddleware
import aio_pika
import asyncio
import json
import pika
from aio_pika.patterns import RPC
from fastapi.concurrency import run_in_threadpool
import logging
import logstash

def sendlog(email):
    logger = logging.getLogger("logit")
    logger.setLevel(logging.INFO)
    logger.addHandler(
        logstash.LogstashHandler(
            'da48e381-9738-4c7c-9d02-6241f100a73b-ls.logit.io', 
            18515, 
            version=1))

    logger.addHandler(logging.StreamHandler())

    logger.info(f'[upload-file-service-publisher] published to rabbitmq from user: {email} ')
    

async def publisher(publish_data) -> None:
    connection = pika.BlockingConnection(pika.ConnectionParameters(host="rabbitmq"))
    channel = connection.channel()
    channel.queue_declare(queue="upload_text_file")
    channel.basic_publish(
        exchange="", routing_key="upload_text_file", body=publish_data.encode()
    )
    print("ini kirim")
    connection.close()
    


app = FastAPI()
origins = [
    "http://localhost",
    "http://localhost:8080",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.post("/uploadfile/{email}")
async def create_upload_file(email: str, file: UploadFile):
    filename = file.filename
    text_data = file.file.read().decode("utf_8")
    publish_data = {"email": email, "filename": filename, "text_data": text_data}

    json_str_object = json.dumps(publish_data)

    await publisher(json_str_object)
    sendlog(email=email)
    return {"filename": file.filename}
