from encodings import utf_8
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import asyncio
import json
import grpc
import text_processing_pb2
import text_processing_pb2_grpc
import pika
import logging
import logstash
channel = grpc.insecure_channel("text-processing-grpc:5000")
stub = text_processing_pb2_grpc.TextProcessingStub(channel)

def sendlog(data):
    logger = logging.getLogger("logit")
    logger.setLevel(logging.INFO)
    logger.addHandler(
        logstash.LogstashHandler(
            'da48e381-9738-4c7c-9d02-6241f100a73b-ls.logit.io', 
            18515, 
            version=1))

    logger.addHandler(logging.StreamHandler())

    logger.info(f'[upload-file-service-consumer] consumed file: {data}')

def process_message(ch, method, properties, body) -> None:

    message_body = body.decode("utf_8")
    message_dict = json.loads(message_body)

    try:
        response = stub.process_text(
            text_processing_pb2.TextDataRequest(
                email=message_dict["email"],
                filename=message_dict["filename"],
                text_data=message_dict["text_data"],
            )
        )
        print(response)
        sendlog(response)

    # Catch any raised errors by grpc.
    except grpc.RpcError as e:
        print("Error raised: " + e.details())


async def consumer() -> None:

    connection = pika.BlockingConnection(pika.ConnectionParameters(host="rabbitmq"))
    channel = connection.channel()
    channel.queue_declare(queue="upload_text_file")

    channel.basic_consume(
        queue="upload_text_file", on_message_callback=process_message, auto_ack=True
    )
    channel.start_consuming()

    return connection


app = FastAPI()
origins = [
    "http://localhost",
    "http://localhost:8080",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.on_event("startup")
async def startup():
    asyncio.ensure_future(consumer())


@app.on_event("startup")
async def init_grpc():
    channel = grpc.insecure_channel("text-processing-grpc:5000")
    stub = text_processing_pb2_grpc.TextProcessingStub(channel)


@app.get("/")
async def root():
    return {"message": "Hello World"}
