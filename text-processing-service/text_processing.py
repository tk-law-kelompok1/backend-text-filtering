from fastapi import FastAPI
from db import database, HateWords

app = FastAPI()

@app.post("/process-text")
async def process_text(email: str, filename: str, text_data: str):
    hate_words = await HateWords.objects.all()

    text = text_data.split()

    count = 0
    for kata in range(len(text)):
        count = count + 1

    counthw = 0
    for kata in text:
        for hw in range(len(hate_words)):
            if kata == hate_words[hw].words:
                counthw = counthw + 1

    return {"email": email, "filename": filename, "word count": count, "number of hate words": counthw}



@app.on_event("startup")
async def startup():
    if not database.is_connected:
        await database.connect()


@app.on_event("shutdown")
async def shutdown():
    if database.is_connected:
        await database.disconnect()

