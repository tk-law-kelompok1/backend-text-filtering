import databases
import ormar
import sqlalchemy

from config import settings

database = databases.Database(settings.db_url)
metadata = sqlalchemy.MetaData()


class BaseMeta(ormar.ModelMeta):
    metadata = metadata
    database = database


class HateWords(ormar.Model):
    class Meta(BaseMeta):
        tablename = "hate_words"

    words: str = ormar.String(primary_key=True, max_length=100)


engine = sqlalchemy.create_engine(settings.db_url)
metadata.create_all(engine)