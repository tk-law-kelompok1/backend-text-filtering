import grpc
import text_processing_pb2
import text_processing_pb2_grpc
from utils import HATE_WORDS
import json
import asyncio

_cleanup_coroutines = []


class TextProcessingServiceGRPC(text_processing_pb2_grpc.TextProcessingServicer):
    async def process_text(self, request, context):
        email = request.email
        filename = request.filename
        text_data = request.text_data
        text = text_data.split()
        hate_words_list = HATE_WORDS.split()

        count = 0
        for kata in range(len(text)):
            count = count + 1

        counthw = 0
        for kata in text:
            for hw in range(len(hate_words_list)):
                if kata == hate_words_list[hw]:
                    counthw = counthw + 1

        data_dict = {
            "email": email,
            "filename": filename,
            "words_count": count,
            "hate_words_count": counthw,
        }

        data_json = json.dumps(data_dict)
        response_data = text_processing_pb2.TextDataResponse()
        response_data.result = data_json
        return response_data


async def main():
    MAX_MESSAGE_LENGTH = 64194304
    server = grpc.aio.server(
        options=[
            ('grpc.max_receive_message_length', MAX_MESSAGE_LENGTH)
        ]
    )
    text_processing_pb2_grpc.add_TextProcessingServicer_to_server(
        TextProcessingServiceGRPC(), server
    )
    print("Starting server. Listening on port 5000.")
    server.add_insecure_port("[::]:5000")
    await server.start()

    async def server_graceful_shutdown():
        await server.stop(5)

    _cleanup_coroutines.append(server_graceful_shutdown())
    await server.wait_for_termination()


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(main())
    finally:
        loop.run_until_complete(*_cleanup_coroutines)
        loop.close()
